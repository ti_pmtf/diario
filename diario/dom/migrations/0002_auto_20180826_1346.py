# Generated by Django 2.1 on 2018-08-26 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dom', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='edicoes',
            options={'ordering': ['numero', 'dt_publicacao', 'status'], 'verbose_name': 'Edição', 'verbose_name_plural': 'Edições'},
        ),
        migrations.AlterField(
            model_name='edicoes',
            name='numero',
            field=models.IntegerField(max_length=100, verbose_name='Edição'),
        ),
    ]
