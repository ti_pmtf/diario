from django.db import models

# Create your models here.
class Edicoes(models.Model):
    """Model representing an author."""
    id = models.AutoField(primary_key=True)
    numero = models.IntegerField("Edição", unique=True, max_length=100, error_messages={'unique': u'O número desta edição já existe.'})
    dt_publicacao = models.DateField("Data da Edição", null=True, blank=False, unique=True, error_messages={'unique': u'Já existe uma edição cadastrada para esta data.'})
    status = models.BooleanField("Publicar edição?", default=False)

    class Meta:
        ordering = ['numero', 'dt_publicacao', 'status']
        verbose_name = 'Edição'
        verbose_name_plural = 'Edições'
    
    def __str__(self):
        """String for representing the Model object."""
        return u'%s %s %s' % (self.numero, self.dt_publicacao, self.status)