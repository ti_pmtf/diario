from django.contrib import admin
from dom.models import Edicoes

# Register your models here.
# admin.site.register(Edition)

# Define the admin class
class EdicoesAdmin(admin.ModelAdmin):
    list_display = ('numero', 'dt_publicacao', 'status')
    search_fields = ('numero', 'dt_publicacao')

# Register the admin class with the associated model
admin.site.register(Edicoes, EdicoesAdmin)
